package com.rms.orderservice.controller;

import com.rms.orderservice.dto.OrderDto;
import com.rms.orderservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/order")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService ;
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String placeOrder(@RequestBody OrderDto orderDto) {
        orderService.placeOrder(orderDto);
        return "Order Placed Successfully" ;
    }
}
