package com.rms.orderservice.service;

import com.rms.orderservice.dto.InventoryResponseDto;
import com.rms.orderservice.dto.OrderDto;
import com.rms.orderservice.dto.OrderLineItemDto;
import com.rms.orderservice.model.Order;
import com.rms.orderservice.model.OrderLineItem;
import com.rms.orderservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderService {
    private final OrderRepository orderRepository ;
    private final WebClient.Builder webClientBuilder ;

    public void placeOrder(OrderDto orderDto) {
        Order order = new Order();
        order.setOrderNumber(UUID.randomUUID().toString());

        List<OrderLineItem> orderLineItems = orderDto
                .getOrderLineItemDtos()
                .stream()
                .map(this::DtoToModel)
                .toList();

        order.setOrderLineItemList(orderLineItems);

        List<String> skuCodes = order.getOrderLineItemList().stream().map(OrderLineItem::getSkuCode).toList() ;
        // check if the product is in stock
        InventoryResponseDto[] inventoryResponses = webClientBuilder
                .build()
                .get()
                .uri("http://inventory-service/api/v1/inventory",uriBuilder -> uriBuilder.queryParam("skuCodes",skuCodes).build())
                .retrieve()
                .bodyToMono(InventoryResponseDto[].class)
                .block();

        boolean allProductsInStock = Arrays.stream(Objects.requireNonNull(inventoryResponses)).allMatch(InventoryResponseDto::isQuantityInStock) ;

        if(Boolean.TRUE.equals(allProductsInStock))
            orderRepository.save(order) ;
        else {
            throw new IllegalArgumentException("Product is not in stock") ;
        }

    }

    private OrderLineItem DtoToModel(OrderLineItemDto orderLineItemDto) {
        return OrderLineItem.builder().skuCode(orderLineItemDto.getSkuCode())
                .price(orderLineItemDto.getPrice())
                .quantity(orderLineItemDto.getQuantity()).build();
    }
}
